﻿namespace BaseProject.Utility
{
    public class PhoneNumberHelper
    {
        public static string FixPhoneNumber(string phoneNumber)
        {
            if (string.IsNullOrEmpty(phoneNumber)) return string.Empty;
            phoneNumber = phoneNumber.TrimEnd();
            phoneNumber = phoneNumber.TrimStart('0', '+', ' ');
            phoneNumber = phoneNumber.StartsWith("98") ? $"+{phoneNumber}" : $"+98{phoneNumber}";
            return phoneNumber;
        }
    }
}