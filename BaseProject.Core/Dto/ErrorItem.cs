﻿namespace BaseProject.Core.Dto
{
    public class ErrorItem
    {
        public int Code { get; set; }

        public string Message { get; set; }

        public ErrorItem()
        {
        }

        /// <summary>
        /// Creates a new Error Item with error code and message
        /// </summary>
        /// <param name="code"></param>
        /// <param name="message"></param>
        public ErrorItem(int code, string message)
        {
            Code = code;
            Message = message;
        }
    }
}