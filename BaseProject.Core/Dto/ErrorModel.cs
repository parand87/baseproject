﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BaseProject.Core.Dto
{
    public class ErrorModel
    {
        public int Code { get; set; }

        public string Message { get; set; }

        public List<ErrorItem> Errors { get; set; } = new List<ErrorItem>();

        public ErrorModel(int code, string message, IEnumerable<ErrorItem> errors)
        {
            Code = code;
            Message = message;
            Errors.AddRange(errors);
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}