﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BaseProject.Core.Dto
{
    public class OperationResult
    {
        protected readonly List<ErrorItem> _errors = new List<ErrorItem>();

        public bool Succeeded { get; protected set; }

        public IEnumerable<ErrorItem> ErrorItems => _errors;

        public IEnumerable<string> ErrorMessages
        {
            get { return _errors.Select(er => er.Message); }
        }

        public static OperationResult Success()
        {
            var operationResult = new OperationResult() { Succeeded = true };
            return operationResult;
        }

        public static OperationResult<T> Success<T>(T data)
        {
            if (data == null)
                throw new ArgumentNullException(nameof(data));

            var operationResult = new OperationResult<T>(data, true);
            return operationResult;
        }

        public static OperationResult Failed(int code, string errorMessage)
        {
            var operationResult = new OperationResult { Succeeded = false };
            operationResult._errors.Add(new ErrorItem(code, errorMessage));

            return operationResult;
        }

        public static OperationResult Failed(params ErrorItem[] errors)
        {
            var operationResult = new OperationResult { Succeeded = false };
            if (errors != null)
                operationResult._errors.AddRange(errors);

            return operationResult;
        }

        public static OperationResult Failed(IEnumerable<ErrorItem> errors)
        {
            var operationResult = new OperationResult { Succeeded = false };
            if (errors != null)
                operationResult._errors.AddRange(errors);

            return operationResult;
        }
    }

    public class OperationResult<T> : OperationResult
    {
        public T Data { get; private set; }

        public OperationResult()
        {
        }

        protected internal OperationResult(T data, bool success)
        {
            Data = data;
            Succeeded = success;
        }

        public new static OperationResult<T> Failed(int code, string errorMessage)
        {
            var operationResult = new OperationResult<T> { Succeeded = false };
            operationResult._errors.Add(new ErrorItem(code, errorMessage));

            return operationResult;
        }

        public new static OperationResult<T> Failed(params ErrorItem[] errors)
        {
            var operationResult = new OperationResult<T> { Succeeded = false };
            if (errors != null)
                operationResult._errors.AddRange(errors);

            return operationResult;
        }

        public new static OperationResult<T> Failed(IEnumerable<ErrorItem> errors)
        {
            var operationResult = new OperationResult<T> { Succeeded = false };
            if (errors != null)
                operationResult._errors.AddRange(errors);

            return operationResult;
        }
    }
}