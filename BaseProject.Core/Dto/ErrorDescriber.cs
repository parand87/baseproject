﻿using System.Globalization;
using Microsoft.AspNetCore.Identity;
using BaseProject.Core.Resources;

namespace BaseProject.Core.Dto
{
    public class ErrorDescriber : IdentityErrorDescriber
    {
        public ErrorItem InvalidPhoneNumber(string phoneNumber)
        {
            return new ErrorItem
            {
                Code = 101,
                Message = string.Format(CultureInfo.CurrentCulture, ErrorResource.InvalidPhoneNumber, phoneNumber)
            };
        }

        public ErrorItem InvalidVerificationCode()
        {
            return new ErrorItem
            {
                Code = 102,
                Message = ErrorResource.InvalidVerificationCode
            };
        }

        public ErrorItem InvalidVerificationToken()
        {
            return new ErrorItem
            {
                Code = 103,
                Message = ErrorResource.InvalidVerificationToken
            };
        }

        public ErrorItem UserNotFound(string userName)
        {
            return new ErrorItem
            {
                Code = 104,
                Message = string.Format(CultureInfo.CurrentCulture, ErrorResource.UserNotFound, userName)
            };
        }

        public override IdentityError DuplicateUserName(string userName)
        {
            return new IdentityError
            {
                Code = "204",
                Description = string.Format(CultureInfo.CurrentCulture, ErrorResource.DuplicateUserName, userName)
            };
        }

        public override IdentityError PasswordRequiresDigit()
        {
            return new IdentityError
            {
                Code = "205",
                Description = ErrorResource.PasswordRequiresDigit
            };
        }

        public override IdentityError PasswordRequiresLower()
        {
            return new IdentityError
            {
                Code = "206",
                Description = ErrorResource.PasswordRequiresLower
            };
        }

        public override IdentityError PasswordRequiresNonAlphanumeric()
        {
            return new IdentityError
            {
                Code = "207",
                Description = ErrorResource.PasswordRequiresNonAlphanumeric
            };
        }

        public override IdentityError PasswordRequiresUpper()
        {
            return new IdentityError
            {
                Code = "208",
                Description = ErrorResource.PasswordRequiresUpper
            };
        }

        public override IdentityError PasswordTooShort(int length)
        {
            return new IdentityError
            {
                Code = "209",
                Description = ErrorResource.PasswordTooShort
            };
        }

        public override IdentityError PasswordMismatch()
        {
            return new IdentityError
            {
                Code = "210",
                Description = ErrorResource.PasswordMismatch
            };
        }

        public ErrorItem LeadAlreadyConverted()
        {
            return new ErrorItem
            {
                Code = 302,
                Message = ErrorResource.LeadAlreadyConverted
            };
        }

        public ErrorItem BadRequest()
        {
            return new ErrorItem
            {
                Code = 400,
                Message = ErrorResource.BadRequest
            };
        }

        public ErrorItem UnsupportedMediaType()
        {
            return new ErrorItem
            {
                Code = 415,
                Message = ErrorResource.UnsupportedMediaType
            };
        }

        public ErrorItem Unauthorized()
        {
            return new ErrorItem
            {
                Code = 401,
                Message = ErrorResource.Unauthorized
            };
        }

        public ErrorItem Forbidden()
        {
            return new ErrorItem
            {
                Code = 403,
                Message = ErrorResource.Forbidden
            };
        }
    }
}