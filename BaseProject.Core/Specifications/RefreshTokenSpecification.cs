﻿using System;
using BaseProject.Core.Entities;

namespace BaseProject.Core.Specifications
{
    public class RefreshTokenSpecification : BaseSpecification<RefreshToken>
    {
        public RefreshTokenSpecification(string token)
            : base(p => p.Token == token && p.ExpiresUtc > DateTime.UtcNow)
        {
        }
    }
}