﻿using System.Linq;
using Microsoft.AspNetCore.Identity;
using BaseProject.Core.Dto;

namespace BaseProject.Core.Extentions
{
    public static class IdentityResultExtensions
    {
        public static OperationResult ToOperationResult(this IdentityResult result)
        {
            if (result.Succeeded)
            {
                return OperationResult.Success();
            }


            var errors = result.Errors.Select(error => new ErrorItem(ToIntCode(error.Code), error.Description));
            return OperationResult.Failed(errors);
        }

        private static int ToIntCode(string code)
        {
            var result = int.TryParse(code, out var intCode);
            return result ? intCode : 400;
        }
    }
}