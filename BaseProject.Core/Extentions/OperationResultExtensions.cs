﻿using System.Linq;
using System.Net;
using BaseProject.Core.Dto;
using BaseProject.Core.Resources;

namespace BaseProject.Core.Extentions
{
    public static class OperationResultExtensions
    {
        public static ErrorModel ToErrorModel(this OperationResult result, int code, string message)
        {
            return new ErrorModel(code, message, result.ErrorItems);
        }

        public static ErrorModel ToErrorModel(this OperationResult result, int code)
        {
            return result.ToErrorModel(code, result.ErrorItems.First().Message);
        }

        public static ErrorModel ToErrorModel(this OperationResult result)
        {
            return result.ToErrorModel((int)HttpStatusCode.BadRequest, result.ErrorItems.First().Message);
        }

        public static ErrorModel ToServerErrorModel(this OperationResult result)
        {
            return result.ToErrorModel((int)HttpStatusCode.InternalServerError, ErrorResource.InternalServerError);
        }
    }
}