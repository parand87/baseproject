﻿using System;

namespace BaseProject.Core.Entities
{
    public class RefreshToken : BaseEntity
    {
        public DateTime IssuedUtc { get; set; }

        public DateTime ExpiresUtc { get; set; }

        public string Token { get; set; }

        public string UserId { get; set; }

        public RefreshToken(DateTime expiresUtc, string userId)
        {
            IssuedUtc = DateTime.UtcNow;
            ExpiresUtc = expiresUtc;
            UserId = userId;
            Token = Guid.NewGuid().ToString("N");
        }

        public RefreshToken()
        {
        }
    }
}