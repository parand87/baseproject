﻿using System;
using BaseProject.Core.Interfaces;

namespace BaseProject.Core.Entities
{
    public class BaseEntity : ISoftDeletable
    {
        public int Id { get; set; }

        public DateTime CreatedAtUtc { get; set; }

        public DateTime ModifiedAtUtc { get; set; }

        public BaseEntity()
        {
        }

        public BaseEntity(DateTime createdAtUtc)
        {
            CreatedAtUtc = createdAtUtc;
            ModifiedAtUtc = createdAtUtc;
        }
    }
}