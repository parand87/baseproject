﻿using System.Threading.Tasks;
using BaseProject.Core.Entities;

namespace BaseProject.Core.Interfaces
{
    public interface IRefreshTokenService
    {
        Task<RefreshToken> GetByTokenAsync(string token);
        Task AddAsync(RefreshToken refreshToken);
    }
}