﻿using System.Threading.Tasks;
using BaseProject.Core.Entities;
using BaseProject.Core.Interfaces;
using BaseProject.Core.Specifications;

namespace BaseProject.Services
{
    public class RefreshTokenService : IRefreshTokenService
    {
        private readonly IAsyncRepository<RefreshToken> _repository;

        public RefreshTokenService(IAsyncRepository<RefreshToken> repository)
        {
            _repository = repository;
        }
        public async Task<RefreshToken> GetByTokenAsync(string token)
        {
            var spec = new RefreshTokenSpecification(token);
            return await _repository.GetSingleBySpecAsync(spec);
        }

        public async Task AddAsync(RefreshToken refreshToken)
        {
            await _repository.AddAsync(refreshToken);
        }
    }
}