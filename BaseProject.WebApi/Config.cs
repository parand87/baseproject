﻿using System.Collections.Generic;
using IdentityModel;
using IdentityServer4.Models;

namespace BaseProject.WebApi
{
    public class Config
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Phone()
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("accountApi", "Account API"),
                new ApiResource("customerApi", "Customer API"),
                new ApiResource{
                    Name = "api",
                    DisplayName = "Test API",
                    UserClaims =
                    {
                        JwtClaimTypes.Id,
                        JwtClaimTypes.Name
                    },
                    Scopes =
                    {
                        new Scope()
                        {
                            Name = "api",
                            DisplayName = "Test API"
                        }
                    }
                }
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            // client credentials client
            return new List<Client>
            {
                new Client
                {
                    ClientId = "customerApiClient",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "accountApi" }
                },

                // resource owner password grant client
                new Client
                {
                    ClientId = "customerSpaClient",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { "customerApi","api" },
                    AllowOfflineAccess = true
                }
            };
        }
    }
}