﻿namespace BaseProject.WebApi.ViewModels
{
    public class LoginViewModel
    {
        public string Username { get; set; }

        public string Password { get; set; }

        public string RefreshToken { get; set; }
    }
}