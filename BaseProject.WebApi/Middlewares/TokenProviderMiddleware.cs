﻿
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using BaseProject.Core.Dto;
using BaseProject.Core.Entities;
using BaseProject.Core.Interfaces;
using BaseProject.Utility;
using BaseProject.WebApi.ViewModels;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace BaseProject.WebApi.Middlewares
{
    public class TokenProviderMiddleware
    {
        private readonly TokenProviderOptions _options;
        private readonly RequestDelegate _next;
        private ErrorDescriber _describer;
        private UserManager<ApplicationUser> _userManager;
        private RoleManager<IdentityRole> _roleManager;
        private IRefreshTokenService _refreshTokenService;
        private readonly JsonSerializerSettings _serializerSettings;

        public TokenProviderMiddleware(IOptions<TokenProviderOptions> options,
            RequestDelegate next

            )
        {
            _options = options.Value;

            ThrowIfInvalidOptions(_options);
            _next = next;

            _serializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.Indented
            };
        }

        public Task Invoke(HttpContext context, ErrorDescriber describer,
            UserManager<ApplicationUser> userManager,
            RoleManager<IdentityRole> roleManager,
            IRefreshTokenService refreshTokenService)
        {
            _describer = describer;
            _userManager = userManager;
            _roleManager = roleManager;
            _refreshTokenService = refreshTokenService;
            context.Response.ContentType = "application/json";
            // If the request path doesn't match, skip
            if (!context.Request.Path.Equals(_options.Path, StringComparison.Ordinal) &&
                !context.Request.Path.Equals(_options.TokenRefreshPath, StringComparison.Ordinal))
            {
                return _next(context);
            }

            // Request must be POST with Content-Type: application/x-www-form-urlencoded
            if (!context.Request.Method.Equals("POST"))
            {
                return BadRequest(context);
            }

            if (string.CompareOrdinal(context.Request.ContentType, "application/json") != 0)
            {
                return UnsupportedMediaType(context);
            }
            //_logger.LogInformation("Handling request: " + context.Request.Path);

            LoginViewModel loginModel = null;
            using (var reader = new StreamReader(context.Request.Body))
            {
                var body = reader.ReadToEnd();
                loginModel = JsonConvert.DeserializeObject<LoginViewModel>(body);
            }

            if (context.Request.Path.Equals(_options.Path, StringComparison.Ordinal))
            {
                var result = AuthenticateWithPassword(loginModel.Username, loginModel.Password).Result;
                if (!result.Succeeded)
                {
                    //if (result.ErrorMessage().Code == ErrorCode.Forbidden)
                    //    return Forbidden(context, result.ErrorMessage().Message);
                    return Unauthorized(context);
                }
                var user = (ApplicationUser)result.Data;
                return GenerateToken(context, user);
            }
            return BadRequest(context);
        }

        /// <summary>
        /// GetAreas this datetime as a Unix epoch timestamp (seconds since Jan 1, 1970, midnight UTC).
        /// </summary>
        /// <param name="date">The date to convert.</param>
        /// <returns>Seconds since Unix epoch.</returns>
        public static long ToUnixEpochDate(DateTime date) => new DateTimeOffset(date).ToUniversalTime().ToUnixTimeSeconds();

        private async Task<OperationResult<ApplicationUser>> AuthenticateWithPassword(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return OperationResult<ApplicationUser>.Failed(_describer.Unauthorized());
            }

            var fixedUsername = PhoneNumberHelper.FixPhoneNumber(username);

            var normalizedUsername = _userManager.NormalizeKey(fixedUsername);

            var user = _userManager.Users.FirstOrDefault(x => x.NormalizedUserName == normalizedUsername);
            if (user == null)
            {
                return OperationResult<ApplicationUser>.Failed(_describer.Unauthorized());
            }

            var isValid = await _userManager.CheckPasswordAsync(user, password);
            if (!isValid)
            {
                return OperationResult<ApplicationUser>.Failed(_describer.Unauthorized());
            }

            //todo: Role Manager!
            //var roleIds = await _userManager.GetRolesAsync(user);
            //var roles = await _roleManager.Roles.Where(x => roleIds.Contains(x.Name)).ToListAsync();

            //var hasAccess = roles.Any(r => r.Name.ToLower().Contains("admin"));
            //return !hasAccess ? OperationResult<Customer>.Failed("شما مجوز دسترسی به این بخش را ندارید!", ErrorCode.Forbidden) : OperationResult.Success(user);
            return OperationResult.Success(user);
        }

        private async Task GenerateToken(HttpContext context, ApplicationUser user, string token = null)
        {
            var identity = await GetIdentity(user);
            if (identity == null)
            {
                await Forbidden(context);
                return;
            }

            var now = DateTime.UtcNow;

            identity.AddClaim(new Claim(JwtRegisteredClaimNames.Sub, user.UserName));
            identity.AddClaim(new Claim(JwtRegisteredClaimNames.Jti, await _options.NonceGenerator()));
            identity.AddClaim(new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(now).ToString(), ClaimValueTypes.Integer64));

            // Create the JWT and write it to a string
            var jwt = new JwtSecurityToken(
                issuer: _options.Issuer,
                audience: _options.Audience,
                claims: identity.Claims, //identity.Claims.Join(claims,), //claims,
                notBefore: now,
                expires: now.Add(_options.TokenExpiration),
                signingCredentials: _options.SigningCredentials);
            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            RefreshToken refreshToken = null;
            if (token == null)
            {
                refreshToken = new RefreshToken(DateTime.UtcNow.Add(_options.RefreshTokenExpiration), identity.Name);
                await _refreshTokenService.AddAsync(refreshToken);
            }
            else
            {
                refreshToken = await _refreshTokenService.GetByTokenAsync(token);
            }

            var response = new
            {
                access_token = encodedJwt,
                expires_in = (int)_options.TokenExpiration.TotalSeconds,
                refresh_token = refreshToken.Token
            };

            // Serialize and return the response
            context.Response.ContentType = "application/json";

            await context.Response.WriteAsync(JsonConvert.SerializeObject(response, _serializerSettings));
        }

        private async Task<ClaimsIdentity> GetIdentity(ApplicationUser user)
        {
            if (user == null)
            {
                return null;
            }

            var roleIds = await _userManager.GetRolesAsync(user);
            var roles = await _roleManager.Roles.Where(x => roleIds.Contains(x.Name)).ToListAsync();

            var claims = new List<Claim>();

            foreach (var item in roles)
            {
                claims.AddRange(await _roleManager.GetClaimsAsync(item));
            }
            return new ClaimsIdentity(new GenericIdentity(user.Id, "Token"), claims.ToArray());
        }

        private async Task BadRequest(HttpContext context)
        {
            await ErrorResponse(context, _describer.BadRequest());
        }

        private async Task UnsupportedMediaType(HttpContext context)
        {
            await ErrorResponse(context, _describer.UnsupportedMediaType());
        }

        private async Task Unauthorized(HttpContext context)
        {
            await ErrorResponse(context, _describer.Unauthorized());
        }

        private async Task Forbidden(HttpContext context)
        {
            await ErrorResponse(context, _describer.Forbidden());
        }

        private async Task ErrorResponse(HttpContext context, ErrorItem error)
        {
            var contentType = context.Request.ContentType;
            context.Response.StatusCode = error.Code;
            string response;
            if (string.CompareOrdinal(contentType, "application/json") == 0)
            {
                var contractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                };
                response = JsonConvert.SerializeObject(error,
                    new JsonSerializerSettings
                    {
                        ContractResolver = contractResolver,
                        Formatting = Formatting.Indented
                    });
            }
            else
            {
                response = error.Message;
            }

            await context.Response.WriteAsync(response);
        }

        private static void ThrowIfInvalidOptions(TokenProviderOptions options)
        {
            if (string.IsNullOrEmpty(options.Path))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Path));
            }

            if (string.IsNullOrEmpty(options.TokenRefreshPath))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.TokenRefreshPath));
            }

            if (string.IsNullOrEmpty(options.Issuer))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Issuer));
            }

            if (string.IsNullOrEmpty(options.Audience))
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.Audience));
            }

            if (options.TokenExpiration == TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(TokenProviderOptions.TokenExpiration));
            }

            if (options.RefreshTokenExpiration == TimeSpan.Zero)
            {
                throw new ArgumentException("Must be a non-zero TimeSpan.", nameof(TokenProviderOptions.RefreshTokenExpiration));
            }

            if (options.SigningCredentials == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.SigningCredentials));
            }

            if (options.NonceGenerator == null)
            {
                throw new ArgumentNullException(nameof(TokenProviderOptions.NonceGenerator));
            }
        }


    }
}