﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using BaseProject.Core.Configuration;
using BaseProject.Core.Entities;
using BaseProject.Infrastructure.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using BaseProject.WebApi.Extensions;
using BaseProject.WebApi.Middlewares;
using BaseProject.WebApi.Resources;

namespace BaseProject.WebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.ConfigureMvcWithLocalization(typeof(SharedResources));
            services.RegisterTypes();
            services.ConfigureDataBase(Configuration);
            services.ConfigureIdentity();
            services.ConfigureIdentityServer();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            services.ConfigureSwagger("Customer API", "v1");
            //services.ConfigureInvalidModelStateResponse();
            services.ConfigureLocalization();
            services.Configure<AppConfig>(options => Configuration.GetSection("AppConfig").Bind(options));

            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("dwYwGsOmGwj6dtORL67v1hIt34WJ6h7X"));


            var tokenValidationParameters = new TokenValidationParameters
            {
                // The signing key must match!
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = signingKey,

                // Validate the JWT Issuer (iss) claim
                ValidateIssuer = true,
                ValidIssuer = "BaseProject.ir",

                // Validate the JWT Audience (aud) claim
                ValidateAudience = true,
                ValidAudience = "BaseProject",

                // Validate the token expiry
                ValidateLifetime = true,

                // If you want to allow a certain amount of clock drift, set that here:
                ClockSkew = TimeSpan.Zero,
            };

            //services.AddAuthentication(o =>
            //    new JwtBearerOptions
            //    {
            //        //AutomaticAuthenticate = true,
            //        //AutomaticChallenge = true,
            //        TokenValidationParameters = tokenValidationParameters,
            //        Events = new JwtBearerEvents
            //        {
            //            OnAuthenticationFailed = ctx =>
            //            {
            //                ctx.HttpContext.Items.Add("testItem", "testItem");
            //                return Task.CompletedTask;//Task.FromResult(false);
            //            }
            //            ,
            //            OnTokenValidated = ctx => Task.CompletedTask
            //        }
            //    });
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(x =>
                {
                    x.RequireHttpsMetadata = false;
                    x.SaveToken = true;
                    x.TokenValidationParameters = tokenValidationParameters;
                    //x.TokenValidationParameters = new TokenValidationParameters
                    //{
                    //    ValidateIssuerSigningKey = true,
                    //    IssuerSigningKey = new SymmetricSecurityKey(key),
                    //    ValidateIssuer = false,
                    //    ValidateAudience = false
                    //};;
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, UserManager<ApplicationUser> userManager)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                    .AllowAnyHeader()
                    .AllowAnyMethod());

            app.ConfigureSwagger("Customer API V1");
            app.UseStaticFiles();
            app.ConfigureExceptionHandler();
            app.UseStatusCodePagesWithReExecute("/api/errors/{0}");
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);
            app.UseAuthentication();

            DbInitializer.SeedUsers(userManager);


            var signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("dwYwGsOmGwj6dtORL67v1hIt34WJ6h7X"));

            app.UseSimpleTokenProvider(new TokenProviderOptions
            {
                Path = "/api/token",
                TokenRefreshPath = "/api/token/refresh",
                Audience = "CrowFunding",
                Issuer = "crowdfunding.ir",
                TokenExpiration = TimeSpan.FromDays(10),
                RefreshTokenExpiration = TimeSpan.FromDays(30),
                SigningCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256),
            });


            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
