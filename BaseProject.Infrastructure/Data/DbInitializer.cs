﻿using Microsoft.AspNetCore.Identity;
using BaseProject.Core.Entities;

namespace BaseProject.Infrastructure.Data
{
    public static class DbInitializer
    {
        public static void SeedUsers(UserManager<ApplicationUser> userManager)
        {
            if (userManager.FindByNameAsync("admin").Result != null) return;
            var user = new ApplicationUser
            {
                UserName = "admin",
                FirstName = "admin"
            };

            var result = userManager.CreateAsync(user, "asdzplk@123").Result;
        }
    }
}